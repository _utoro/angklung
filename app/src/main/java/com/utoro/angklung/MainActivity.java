package com.utoro.angklung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Build;
import android.media.AudioManager;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private SoundPool soundPool;
    private int soundDo, soundRe, soundMi, soundFa, soundSol, soundLa, soundSi, soundDo2;
    float actVolume, maxVolume, volume;
    AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        actVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        if (actVolume <= 0) actVolume = (float) 1.0;
        volume = actVolume / maxVolume;

        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(8)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
        }

        soundDo = soundPool.load(this, R.raw.long_do1, 1);
        soundRe = soundPool.load(this, R.raw.long_re, 1);
        soundMi = soundPool.load(this, R.raw.long_mi, 1);
        soundFa = soundPool.load(this, R.raw.long_fa, 1);
        soundSol = soundPool.load(this, R.raw.long_sol, 1);
        soundLa = soundPool.load(this, R.raw.long_la, 1);
        soundSi = soundPool.load(this, R.raw.long_si, 1);
        soundDo2 = soundPool.load(this, R.raw.long_do2, 1);
    }


    public void playDo(View view) {
        soundPool.play(soundDo, volume, volume, 0, 0, 1);
//        soundPool.pause(sound3StreamId);
//        soundPool.autoPause();
    }

    public void playRe(View view) {
        soundPool.play(soundRe, volume, volume, 0, 0, 1);
    }

    public void playMi(View view) {
//        sound3StreamId = soundPool.play(soundMi, 1, 1, 0, 0, 1);
        soundPool.play(soundMi, volume, volume, 0, 0, 1);
    }

    public void playFa(View view) {
        soundPool.play(soundFa, volume, volume, 0, 0, 1);
    }

    public void playSol(View view) {
        soundPool.play(soundSol, volume, volume, 0, 0, 1);
    }

    public void playLa(View view) {
        soundPool.play(soundLa, volume, volume, 0, 0, 1);
    }

    public void playSi(View view) {
        soundPool.play(soundSi, volume, volume, 0, 0, 1);
    }

    public void playDo2(View view) {
        soundPool.play(soundDo2, volume, volume, 0, 0, 1);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundPool.release();
        soundPool = null;
    }
}
